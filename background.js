// Allow cookies set by *.facebook.com on all *.facebook.com web pages
chrome.contentSettings.cookies.set({
  primaryPattern: '*://*.facebook.com/*',
  secondaryPattern: '*://*.facebook.com/*',
  setting: 'allow' });

// Block cookies set by *.facebook.com everywhere else
chrome.contentSettings.cookies.set({
  primaryPattern: '*://*.facebook.com/*',
  secondaryPattern: '<all_urls>',
  setting: 'block' });
